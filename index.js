const { execSync } = require('node:child_process');
const { join, loadJSON, loadYAML, saveYAML, existsSync, relative, copy, ensureDirectory, dirname, findAllFiles } = require("nanoweb-fs");

function deepMerge(target, source) {
	// Create a deep copy of the target
	// copied from 'nanoweb/data' to avoid dependency of the whole platform.
	let newTarget = JSON.parse(JSON.stringify(target));

	for (const key in source) {
		if (source.hasOwnProperty(key)) {
			if (source[key] && typeof source[key] === 'object') {
				if (Array.isArray(source[key])) {
					// Replace with a copy of the array
					newTarget[key] = source[key].slice();
				} else {
					// Perform a deep merge on a copy of the object
					newTarget[key] = newTarget[key] || {};
					newTarget[key] = deepMerge(newTarget[key], source[key]);
				}
			} else {
				newTarget[key] = source[key];
			}
		}
	}
	return newTarget;
}

function getThemes() {
	if (!loadedThemes) loadedThemes = loadYAML(join(__dirname, 'themes.yaml'));
	return loadedThemes;
}

function findPackageMain(dir, name) {
	let currentDir = dir;
	let i = 0;
	do {
		const packageDir = join(currentDir, 'node_modules', name);
		const packageFile = join(packageDir, 'package.json');
		if (existsSync(packageFile)) {
			const json = loadJSON(packageFile);
			const moduleFile = join(packageDir, json?.main);
			if (existsSync(moduleFile)) {
				return moduleFile;
			}
			return false;
		}
		currentDir = dirname(currentDir);
	} while (currentDir && ++i < 9);
	return false;
}

/**
 * @link nanoweb/runtime
 */
const DEFAULT_THEME_INFO = {
	copy: false,
	merge: true,
	name: null,
	dir: null,
};
const INIT_INFO_DEFAULT = {
	themes: {},
};

function installFromNpm(url, theme, runtime) {
	const ROOT_DIR = runtime.get('ROOT_DIR');
	const DATA_DIR = runtime.get('DATA_DIR');
	const STATIC_DIR = runtime.get('STATIC_DIR');
	const opts = runtime.get('THEMES_DIRS')[theme];
	const name = url.split('/').pop();
	const command = `npm install ${name}`;
	// dir is nanoweb-llm-server/themes/nano
	// but nanoweb-llm-server/package.json is updated with installed package
	execSync(command, { cwd: ROOT_DIR, stdio: 'ignore', shell: true });
	// finding closest node_modules/${name}/package.json from current to root.
	const mainFile = findPackageMain(ROOT_DIR, name);
	if (!mainFile) {
		throw new Error(`Theme [${name}] package main file not found in ${ROOT_DIR}`);
	}
	let result = true;
	// { ...DEFAULT_THEME_INFO, ...opts };
	// "/Users/yaro/Projects/npm/nanoweb-llm-server/node_modules/nanoweb-theme-nano/index.js"
	const main = require(mainFile);
	if (opts.copy && main?.rules?.views) {
		const viewsDir = join(dirname(mainFile), main.rules.views);
		ensureDirectory(opts.dir);
		if (existsSync(viewsDir)) {
			copy(viewsDir, join(opts.dir, 'views'), true);
		}
	} else {
		result = dirname(mainFile);
	}
	if (opts.merge && main?.rules?.templates) {
		const templatesDir = join(dirname(mainFile), main.rules.templates);
		const files = findAllFiles(templatesDir);
		for (const file of files) {
			const rel = relative(templatesDir, file);
			if (rel.startsWith('data/')) {
				// merge all the files with the data or create new
				const relFile = rel.slice('data/'.length);
				const relPath = join(DATA_DIR, relFile);
				ensureDirectory(dirname(relPath));
				const template = loadYAML(file);
				if (existsSync(relPath)) {
					let target = null;
					try {
						target = loadYAML(relPath);
					} catch (err) {
						// file does not exist, probably.
					}
					if (target) {
						const merged = deepMerge(template, target);
						saveYAML(relPath, merged);
					} else {
						saveYAML(relPath, template);
					}
				} else {
					saveYAML(relPath, template);
				}
			} else if (rel.startsWith('static/')) {
				// copy every item to the static if does not exist
				const relFile = rel.slice('static/'.length);
				const relPath = join(STATIC_DIR, relFile);
				if (!existsSync(relPath)) {
					ensureDirectory(dirname(relPath));
					copy(file, relPath);
				}
			}
		}
	}
	return result;
}

function installTheme(theme, runtime) {
	const themes = getThemes();
	if (!themes[theme]) {
		throw new Error(`Theme ${theme} not found`);
	}
	const url = themes[theme];
	if (url.startsWith('https://www.npmjs.com/package/')) {
		return installFromNpm(url, theme, runtime);
	}
	return false;
}
let loadedThemes = null;

module.exports = {
	getThemes,
	installTheme,
};
