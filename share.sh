#!/bin/bash

# Color and symbol constants
GREEN='\x1b[92m'     # Green Text       ✓
RED='\x1b[91m'       # Red Text         ×
YELLOW='\x1b[93m'    # Yellow Text
BG_YELLOW='\x1b[43m' # Yellow Background
RESET='\x1b[0m'      # Reset to default
OK='✓'
FAIL='×'
NANO='·' # ASCII Extended middle dot

# Step 1: Check for uncommitted changes
if ! git diff-index --quiet HEAD --; then
	echo -e " ${RED}${FAIL} Uncommitted changes found. ${RESET}"
	echo -e "   Please commit or stash them before publishing."
	echo -e ""
	exit 1
fi
echo -e " ${GREEN}${OK} No uncommitted changes.${RESET}"

# Step 2: Pull latest changes
git pull origin $(git rev-parse --abbrev-ref HEAD)
if [ $? -ne 0 ]; then
	echo -e " ${RED}${FAIL} Failed to pull latest changes.${RESET}"
	exit 1
fi
echo -e " ${GREEN}${OK} Latest changes pulled.${RESET}"

# Step 3: Run tests
npm test
if [ $? -ne 0 ]; then
	echo -e " ${RED}${FAIL} Tests failed. Aborting publish.${RESET}"
	exit 1
fi
echo -e " ${GREEN}${OK} Tests passed.${RESET}"

# Step 4: Clean build artifacts
npm run clean
if [ $? -ne 0 ]; then
	echo -e " ${RED}${FAIL} Clean failed.${RESET}"
	exit 1
fi
echo -e " ${GREEN}${OK} Build artifacts cleaned.${RESET}"

# Step 5: Run linter
npm run lint
if [ $? -ne 0 ]; then
	echo -e " ${RED}${FAIL} Linting failed. Aborting publish.${RESET}"
	exit 1
fi
echo -e " ${GREEN}${OK} Linting passed.${RESET}"

# Step 6: Build project
npm run build
if [ $? -ne 0 ]; then
	echo -e " ${RED}${FAIL} Build failed. Aborting publish.${RESET}"
	exit 1
fi
echo -e " ${GREEN}${OK} Build succeeded.${RESET}"

# Step 7: Remove unnecessary files
# Optionally, manually remove files if needed

# Step 8: Verify .npmignore
# Ensure your .npmignore is properly configured

# Step 9: Tag the release (optional)
VERSION=$(node -p "require('./package.json').version")
git tag -a "v$VERSION" -m "Release version $VERSION"
if [ $? -ne 0 ]; then
	echo -e " ${RED}${FAIL} Failed to tag the release.${RESET}"
	exit 1
fi
git push origin --tags
if [ $? -ne 0 ]; then
	echo -e " ${RED}${FAIL} Failed to push tags.${RESET}"
	exit 1
fi
echo -e " ${GREEN}${OK} Release tagged and pushed.${RESET}"

# Finally, publish the package
npm publish
if [ $? -ne 0 ]; then
	echo -e " ${RED}${FAIL} Publishing failed.${RESET}"
	exit 1
fi
echo -e " ${GREEN}${OK} Package published successfully.${RESET}"
