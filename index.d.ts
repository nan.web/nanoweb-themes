declare const GREEN: string;  // Green Text       ✓
declare const RED: string;  // Red Text         ×
declare const YELLOW: string;  // Yellow Text
declare const BG_YELLOW: string;  // Yellow Background
declare const RESET: string;   // Reset to default 
declare const OK: string;
declare const FAIL: string;
declare const NANO: string; // ASCII Extended middle dot

/**
 * Deeply merges two objects.
 * @param {object} target - The target object.
 * @param {object} source - The source object.
 * @returns {object} The merged object.
 */
declare function deepMerge(target: object, source: object): object;

/**
 * Loads and returns the themes from a YAML file.
 * @returns {any} The loaded themes.
 */
declare function getThemes(): any;

/**
 * Finds the main file of a package by searching through node_modules.
 * @param {string} dir - The directory to start the search from.
 * @param {string} name - The name of the package.
 * @returns {string | false} The path to the main file or false if not found.
 */
declare function findPackageMain(dir: string, name: string): string | false;

declare const DEFAULT_THEME_INFO: {
	copy: boolean;
	merge: boolean;
	name: string | null;
	dir: string | null;
};

declare const INIT_INFO_DEFAULT: {
	themes: Record<string, any>;
};

/**
 * Installs a theme from npm and processes its files according to the provided runtime configuration.
 * @param {string} url - The URL of the npm package.
 * @param {string} theme - The name of the theme.
 * @param {any} runtime - The runtime configuration.
 * @returns {string | boolean} The directory of the installed theme or true if files were copied, false otherwise.
 * @throws {Error} If the main file of the theme package is not found.
 */
declare function installFromNpm(url: string, theme: string, runtime: any): string | boolean;

/**
 * Installs a theme by its name using the runtime configuration.
 * @param {string} theme - The name of the theme.
 * @param {any} runtime - The runtime configuration.
 * @returns {string | boolean} The result of the theme installation.
 * @throws {Error} If the theme is not found in the loaded themes.
 */
declare function installTheme(theme: string, runtime: any): string | boolean;

declare let loadedThemes: any;

export {
	deepMerge,
	getThemes,
	findPackageMain,
	DEFAULT_THEME_INFO,
	INIT_INFO_DEFAULT,
	installFromNpm,
	installTheme,
	loadedThemes
};