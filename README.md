# Themes for the nan•web websites

## Features
- Responsive design
- Customizable layouts
- Easy to integrate

## Installation

```sh
git clone https://gitlab.com/nan.web/nanoweb-themes.git
cd nanoweb-themes
npm install
```

### Usage

```sh
npm run build
```
